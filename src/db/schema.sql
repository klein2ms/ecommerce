﻿SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- Create the Get Stored Procs

CREATE PROCEDURE dbo.Product_GetList
AS

SET NOCOUNT ON

SELECT *
FROM dbo.Product

GO

CREATE PROCEDURE dbo.Customer_GetList
AS

SET NOCOUNT ON

SELECT *
FROM dbo.Customer

GO

CREATE PROCEDURE dbo.Offer_GetList
AS

SET NOCOUNT ON

SELECT *
FROM dbo.Offer

GO

CREATE PROCEDURE dbo.Order_GetList
AS

SET NOCOUNT ON

SELECT *
FROM dbo.[Order]

GO

CREATE PROCEDURE dbo.ProductType_GetList
AS

SET NOCOUNT ON

SELECT *
FROM dbo.ProductType

GO

-- Create the Upsert Stored Procs
CREATE PROCEDURE dbo.Product_Upsert
	@ProductId		INT = NULL OUTPUT,
	@Name			NVARCHAR(50),
	@IsActive		BIT,
	@ProductTypeId	INT
AS

SET NOCOUNT ON

BEGIN TRY
BEGIN TRANSACTION

DECLARE @ProductIdTable TABLE (ProductId INT)
DECLARE @IsInsert BIT = 0

IF @ProductId IS NULL
BEGIN
SET @IsInsert = 1
END

-- setup tables for merge
MERGE dbo.Product t
USING
	(
	SELECT
	@ProductId,
	@Name,
	@IsActive,
	@ProductTypeId
	) as s
	(
	ProductId,
	Name,
	IsActive,
	ProductTypeId
	)
ON t.ProductId = s.ProductId

WHEN MATCHED THEN
	UPDATE
	SET t.Name = s.Name,
		t.IsActive = s.IsActive,
		t.ProductTypeId = s.ProductTypeId,
		t.DateModified = GETDATE()

WHEN NOT MATCHED AND @IsInsert = 1 THEN
	INSERT
	(
	Name,
	DateCreated,
	IsActive,
	ProductTypeId,
	DateModified
	)
	VALUES
	(
	s.Name,
	GETDATE(),
	s.IsActive,
	s.ProductTypeId,
	GETDATE()
	)
	OUTPUT Inserted.ProductId INTO @ProductIdTable;

SELECT @ProductId = ProductId FROM @ProductIdTable

COMMIT TRANSACTION
END TRY

BEGIN CATCH
	ROLLBACK TRANSACTION
END CATCH

GO

CREATE PROCEDURE dbo.Customer_Upsert
	@CustomerId		INT = NULL OUTPUT,
	@EmailAddress   NVARCHAR(50),
	@FisrtName		NVARCHAR(100),
	@LastName		NVARCHAR(100)
AS

SET NOCOUNT ON

BEGIN TRY
BEGIN TRANSACTION

DECLARE @CustomerIdTable TABLE (CustomerId INT)
DECLARE @IsInsert BIT = 0

IF @CustomerId IS NULL
BEGIN
SET @IsInsert = 1
END

-- setup tables for merge
MERGE dbo.Customer t
USING
	(
	SELECT
	@CustomerId,
	@EmailAddress,
	@FisrtName,
	@LastName
	) as s
	(
	CustomerId,
	EmailAddress,
	FirstName,
	LastName
	)
ON t.CustomerId = s.CustomerId

WHEN MATCHED THEN
	UPDATE
	SET t.EmailAddress = s.EmailAddress,
		t.FirstName = s.FirstName,
		t.LastName = s.LastName,
		t.DateModified = GETDATE()

WHEN NOT MATCHED AND @IsInsert = 1 THEN
	INSERT
	(
	EmailAddress,
	FirstName,
	LastName,
	DateCreated,	
	DateModified
	)
	VALUES
	(
	s.EmailAddress,
	s.FirstName,
	s.LastName,
	GETDATE(),
	GETDATE()
	)
	OUTPUT Inserted.CustomerId INTO @CustomerIdTable;

SELECT @CustomerId = CustomerId FROM @CustomerIdTable

COMMIT TRANSACTION
END TRY

BEGIN CATCH
	ROLLBACK TRANSACTION
END CATCH

GO


CREATE PROCEDURE dbo.Offer_Upsert
	@OfferId		INT = NULL OUTPUT,
	@ProductId		INT,
	@Description    NVARCHAR(100),
	@Price			DECIMAL(6,2),
	@NumberOfMonths INT
AS

SET NOCOUNT ON

BEGIN TRY
BEGIN TRANSACTION

DECLARE @OfferIdTable TABLE (OfferId INT)
DECLARE @IsInsert BIT = 0

IF @OfferId IS NULL
BEGIN
SET @IsInsert = 1
END

-- setup tables for merge
MERGE dbo.Offer t
USING
	(
	SELECT
	@OfferId,
	@ProductId,
	@Description,
	@Price,
	@NumberOfMonths
	) as s
	(
	OfferId,
	ProductId,
	Description,
	Price,
	NumberOfMonths
	)
ON t.OfferId = s.OfferId

WHEN MATCHED THEN
	UPDATE
	SET t.ProductId = s.ProductId,
		t.Description = s.Description,
		t.Price = s.Price,
		t.NumberOfMonths = s.NumberOfMonths,
		t.DateModified = GETDATE()

WHEN NOT MATCHED AND @IsInsert = 1 THEN
	INSERT
	(
	ProductId,
	Description,
	Price,
	NumberOfMonths,
	DateCreated,	
	DateModified
	)
	VALUES
	(
	s.ProductId,
	s.Description,
	s.Price,
	s.NumberOfMonths,
	GETDATE(),
	GETDATE()
	)
	OUTPUT Inserted.OfferId INTO @OfferIdTable;

SELECT @OfferId = OfferId FROM @OfferIdTable

COMMIT TRANSACTION
END TRY

BEGIN CATCH
	ROLLBACK TRANSACTION
END CATCH

GO

CREATE PROCEDURE dbo.Order_Upsert
	@OrderId		INT = NULL OUTPUT,
	@OfferId		INT,
	@PurchaseDate   DATETIME,
	@CustomerId		INT
AS

SET NOCOUNT ON

BEGIN TRY
BEGIN TRANSACTION

DECLARE @OrderIdTable TABLE (OrderId INT)
DECLARE @IsInsert BIT = 0

IF @OrderId IS NULL
BEGIN
SET @IsInsert = 1
END

-- setup tables for merge
MERGE dbo.[Order] t
USING
	(
	SELECT
	@OrderId,
	@OfferId,
	@PurchaseDate,
	@CustomerId
	) as s
	(
	OrderId,
	OfferId,
	PurchaseDate,
	CustomerId
	)
ON t.OrderId = s.OrderId

WHEN MATCHED THEN
	UPDATE
	SET t.OfferId = s.OfferId,
		t.PurchaseDate = s.PurchaseDate,
		t.CustomerId = s.CustomerId

WHEN NOT MATCHED AND @IsInsert = 1 THEN
	INSERT
	(
	OfferId,
	PurchaseDate,
	CustomerId
	)
	VALUES
	(
	s.OfferId,
	s.PurchaseDate,
	s.CustomerId
	)
	OUTPUT Inserted.OrderId INTO @OrderIdTable;

SELECT @OrderId = OrderId FROM @OrderIdTable

COMMIT TRANSACTION
END TRY

BEGIN CATCH
	ROLLBACK TRANSACTION
END CATCH

GO