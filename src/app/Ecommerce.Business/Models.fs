namespace Ecommerce.Business

open System

module Models =
    
    type Customer =
        { CustomerId : int
          EmailAddress : string
          FirstName : string
          LastName : string
          DateCreated : DateTime
          DateModified : DateTime
        }
    
    type Order =
        { OrderId : int
          OfferId : int
          PurchaseDate : DateTime
          CustomerId : int
        }
    
    type Offer =
        { OfferId : int
          ProductId : int
          Description : string
          Price : decimal
          NumberOfMOnths : int
          DateCreated : DateTime
          DateModified : DateTime
        }
    
    type ProductType =
        { ProductTypeId : int
          Type : string
          DateCreated : DateTime
          DateModified : DateTime
        }
    
    type Product =
        { ProductId : int
          Name : string
          IsActive : bool
          ProductTypeId : int
          DateCreated : DateTime
          DateModified : DateTime
        }