﻿using System.Collections.Generic;
using Ecommerce.Business;
using Ecommerce.Service.Errors;
using Ecommerce.Service.Util;
using Dapper;
using System.Data.SqlClient;
using System.Data;
using System;
using System.Linq;

namespace Ecommerce.Service.Repositories
{
    public class ProductRepository : IRepository<Models.Product>
    {
        private string _connectionString { get; }

        public ProductRepository(string connectionString) => _connectionString = connectionString;

        public ProductRepository() : this(Environment.GetEnvironmentVariable(EnvironmentVariables.DB_CONNECTION_STRING)) { }

        public Result<Error, Models.Product> Add(Models.Product t)
        {
            if (GetAll().Any(p => p.ProductId == t.ProductId))
                return new AlreadyExists<Models.Product>(t);

            using (var connection = new SqlConnection(_connectionString))
            {
                try
                {
                    connection.Open();

                    var p = new DynamicParameters();
                    p.Add("@Name", t.Name);
                    p.Add("@IsActive", t.IsActive);
                    p.Add("@ProductTypeId", t.ProductTypeId);
                    p.Add("@ProductId", dbType: DbType.Int32, direction: ParameterDirection.InputOutput);

                    connection
                        .Query("dbo.Product_Upsert", p, commandType: CommandType.StoredProcedure);

                    return new Models.Product(
                        productId: p.Get<int>("@ProductId"), 
                        name: t.Name, 
                        isActive: t.IsActive, 
                        productTypeId: t.ProductTypeId, 
                        dateCreated: t.DateCreated, 
                        dateModified: t.DateModified);                        
                }
                catch (Exception e)
                {
                    return new DatabaseError<Models.Product>(t, e.Message);
                }
            }
        }

        public IQueryable<Models.Product> GetAll()
        {
            var products = new List<Models.Product>();

            using (var connection = new SqlConnection(_connectionString))
            {
                try
                {
                    connection.Open();

                    products = connection
                        .Query<Models.Product>(
                            "dbo.Product_GetList",
                            commandType: CommandType.StoredProcedure)
                        .ToList();
                }
                catch (Exception)
                {
                    return products.AsQueryable();
                }
            }

            return products.AsQueryable();
        }
    }
}
