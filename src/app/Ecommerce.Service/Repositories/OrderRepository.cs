﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using Ecommerce.Business;
using Ecommerce.Service.Errors;
using Ecommerce.Service.Util;

namespace Ecommerce.Service.Repositories
{
    public class OrderRepository : IRepository<Models.Order>
    {
        public Result<Error, Models.Order> Add(Models.Order t)
        {
            if (GetAll().Any(o => o.OrderId == t.OrderId))
                return new AlreadyExists<Models.Order>(t);

            using (var connection = new SqlConnection(Environment.GetEnvironmentVariable(EnvironmentVariables.DB_CONNECTION_STRING)))
            {
                try
                {
                    connection.Open();

                    var p = new DynamicParameters();
                    p.Add("@OrderId", dbType: DbType.Int32, direction: ParameterDirection.InputOutput);
                    p.Add("@OfferId", t.OfferId);
                    p.Add("@PurchaseDate", t.PurchaseDate);
                    p.Add("@CustomerId", t.CustomerId);

                    connection
                        .Query("dbo.Order_Upsert", p, commandType: CommandType.StoredProcedure);

                    return new Models.Order(
                        orderId: p.Get<int>("@OrderId"),
                        offerId: t.OfferId,
                        purchaseDate: t.PurchaseDate,
                        customerId: t.CustomerId);
                }
                catch (Exception e)
                {
                    return new DatabaseError<Models.Order>(t, e.Message);
                }
            }
        }

        public IQueryable<Models.Order> GetAll()
        {
            var orders = new List<Models.Order>();

            using (var connection = new SqlConnection(Environment.GetEnvironmentVariable(EnvironmentVariables.DB_CONNECTION_STRING)))
            {
                try
                {
                    connection.Open();

                    orders = connection
                        .Query<Models.Order>(
                            "dbo.Order_GetList",
                            commandType: CommandType.StoredProcedure)
                        .ToList();
                }
                catch (Exception)
                {
                    return orders.AsQueryable();
                }
            }

            return orders.AsQueryable();
        }        
    }
}
