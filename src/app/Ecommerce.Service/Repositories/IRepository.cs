﻿using Ecommerce.Service.Errors;
using Ecommerce.Service.Util;
using System.Linq;

namespace Ecommerce.Service.Repositories
{
    public interface IRepository<T>
    {
        IQueryable<T> GetAll();

        Result<Error, T> Add(T t);
    }
}
