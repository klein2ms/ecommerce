﻿using Dapper;
using Ecommerce.Business;
using Ecommerce.Service.Errors;
using Ecommerce.Service.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Ecommerce.Service.Repositories
{
    public class OfferRepository : IRepository<Models.Offer>
    {
        public Result<Error, Models.Offer> Add(Models.Offer t)
        {
            if (GetAll().Any(o => o.OfferId == t.OfferId))
                return new AlreadyExists<Models.Offer>(t);

            using (var connection = new SqlConnection(Environment.GetEnvironmentVariable(EnvironmentVariables.DB_CONNECTION_STRING)))
            {
                try
                {
                    connection.Open();

                    var p = new DynamicParameters();
                    p.Add("@OfferId", dbType: DbType.Int32, direction: ParameterDirection.InputOutput);
                    p.Add("@ProductId", t.ProductId);
                    p.Add("@Description", t.Description);
                    p.Add("@Price", t.Price);
                    p.Add("@NumberOfMonths", t.NumberOfMOnths);

                    connection
                        .Query("dbo.Offer_Upsert", p, commandType: CommandType.StoredProcedure);

                    return new Models.Offer(
                        offerId: p.Get<int>("@OfferId"),
                        productId: t.ProductId,
                        description: t.Description,
                        price: t.Price,
                        numberOfMOnths: t.NumberOfMOnths,
                        dateCreated: t.DateCreated,
                        dateModified: t.DateModified);
                }
                catch (Exception e)
                {
                    return new DatabaseError<Models.Offer>(t, e.Message);
                }
            }
        }

        public IQueryable<Models.Offer> GetAll()
        {
            var offers = new List<Models.Offer>();

            using (var connection = new SqlConnection(Environment.GetEnvironmentVariable(EnvironmentVariables.DB_CONNECTION_STRING)))
            {
                try
                {
                    connection.Open();

                    offers = connection
                        .Query<Models.Offer>(
                            "dbo.Offer_GetList",
                            commandType: CommandType.StoredProcedure)
                        .ToList();
                }
                catch (Exception)
                {
                    return offers.AsQueryable();
                }
            }

            return offers.AsQueryable();
        }
    }    
}
