﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using Ecommerce.Business;
using Ecommerce.Service.Errors;
using Ecommerce.Service.Util;

namespace Ecommerce.Service.Repositories
{
    public class CustomerRepository : IRepository<Models.Customer>
    {
        public Result<Error, Models.Customer> Add(Models.Customer t)
        {
            if (GetAll().Any(c => c.CustomerId == t.CustomerId))
                return new AlreadyExists<Models.Customer>(t);

            using (var connection = new SqlConnection(Environment.GetEnvironmentVariable(EnvironmentVariables.DB_CONNECTION_STRING)))
            {
                try
                {
                    connection.Open();

                    var p = new DynamicParameters();
                    p.Add("@CustomerId", dbType: DbType.Int32, direction: ParameterDirection.InputOutput);
                    p.Add("@EmailAddress", t.EmailAddress);
                    p.Add("@FirstName", t.FirstName);
                    p.Add("@LastName", t.LastName);

                    connection
                        .Query("dbo.Customer_Upsert", p, commandType: CommandType.StoredProcedure);

                    return new Models.Customer(
                        customerId: p.Get<int>("@CustomerId"),
                        emailAddress: t.EmailAddress,
                        firstName: t.FirstName,
                        lastName: t.LastName,
                        dateCreated: t.DateCreated,
                        dateModified: t.DateModified);
                }
                catch (Exception e)
                {
                    return new DatabaseError<Models.Customer>(t, e.Message);
                }
            }
        }

        public IQueryable<Models.Customer> GetAll()
        {
            var customers = new List<Models.Customer>();

            using (var connection = new SqlConnection(Environment.GetEnvironmentVariable(EnvironmentVariables.DB_CONNECTION_STRING)))
            {
                try
                {
                    connection.Open();

                    customers = connection
                        .Query<Models.Customer>(
                            "dbo.Customer_GetList",
                            commandType: CommandType.StoredProcedure)
                        .ToList();
                }
                catch (Exception)
                {
                    return customers.AsQueryable();
                }
            }

            return customers.AsQueryable();
        }
    }    
}
