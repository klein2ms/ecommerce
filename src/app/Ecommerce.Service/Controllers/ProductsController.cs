﻿using Microsoft.AspNetCore.Mvc;
using Ecommerce.Business;
using Ecommerce.Service.Repositories;
using Ecommerce.Service.Util;
using Ecommerce.Service.Errors;

namespace Ecommerce.Service.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class ProductsController : Controller
    {
        private IRepository<Models.Product> _repository { get; }

        public ProductsController(IRepository<Models.Product> repository) => _repository = repository;

        [HttpGet]
        public IActionResult GetAll() =>
            Ok(_repository.GetAll());

        [HttpGet("{id}")]
        public IActionResult Get(int id) =>
            _repository
                .GetAll()
                .TryFind(p => p.ProductId == id)
                .Map<Models.Product, IActionResult>(Ok)
                .Otherwise(NotFound);

        [HttpPost]
        public IActionResult Post([FromBody] Models.Product product) =>
            _repository
                .Add(product)
                .Bind(p => (IActionResult)Created("some link to the new product", p))
                .Reduce(_ => ApiError.Conflict(), error => error is AlreadyExists<Models.Product>)
                .Reduce(_ => ApiError.InternalServerError());
    }
}
