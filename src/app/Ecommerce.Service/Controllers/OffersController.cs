﻿using Ecommerce.Business;
using Ecommerce.Service.Repositories;
using Microsoft.AspNetCore.Mvc;
using Ecommerce.Service.Util;
using Ecommerce.Service.Errors;

namespace Ecommerce.Service.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class OffersController : Controller
    {
        private IRepository<Models.Offer> _repository { get; }

        public OffersController(IRepository<Models.Offer> repository) => _repository = repository;

        [HttpGet]
        public IActionResult GetAll() =>
            Ok(_repository.GetAll());

        [HttpGet("{id}")]
        public IActionResult Get(int id) =>
            _repository
                .GetAll()
                .TryFind(o => o.OfferId == id)
                .Map<Models.Offer, IActionResult>(Ok)
                .Otherwise(NotFound);

        [HttpPost]
        public IActionResult Post([FromBody] Models.Offer offer) =>
            _repository
                .Add(offer)
                .Bind(c => (IActionResult)Created("some link to the new customer", c))
                .Reduce(_ => ApiError.Conflict(), error => error is AlreadyExists<Models.Customer>)
                .Reduce(_ => ApiError.InternalServerError());
    }
}
