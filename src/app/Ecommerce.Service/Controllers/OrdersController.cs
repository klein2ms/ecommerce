﻿using Ecommerce.Business;
using Ecommerce.Service.Errors;
using Ecommerce.Service.Repositories;
using Ecommerce.Service.Util;
using Microsoft.AspNetCore.Mvc;

namespace Ecommerce.Service.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class OrdersController : Controller
    {
        private IRepository<Models.Order> _repository { get; }

        public OrdersController(IRepository<Models.Order> repository) => _repository = repository;

        [HttpGet]
        public IActionResult GetAll() =>
            Ok(_repository.GetAll());

        [HttpGet("{id}")]
        public IActionResult Get(int id) =>
            _repository
                .GetAll()
                .TryFind(o => o.OrderId == id)
                .Map<Models.Order, IActionResult>(Ok)
                .Otherwise(NotFound);

        [HttpPost]
        public IActionResult Post([FromBody] Models.Order order) =>
            _repository
                .Add(order)
                .Bind(p => (IActionResult)Created("some link to the new product", p))
                .Reduce(_ => ApiError.Conflict(), error => error is AlreadyExists<Models.Order>)
                .Reduce(_ => ApiError.InternalServerError());        
    }
}
