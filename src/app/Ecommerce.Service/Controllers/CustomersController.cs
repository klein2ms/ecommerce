﻿using Ecommerce.Business;
using Ecommerce.Service.Repositories;
using Microsoft.AspNetCore.Mvc;
using Ecommerce.Service.Util;
using Ecommerce.Service.Errors;

namespace Ecommerce.Service.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class CustomersController : Controller
    {
        private IRepository<Models.Customer> _repository { get; }

        public CustomersController(IRepository<Models.Customer> repository) => _repository = repository;

        [HttpGet]
        public IActionResult GetAll() =>
            Ok(_repository.GetAll());

        [HttpGet("{id}")]
        public IActionResult Get(int id) =>
            _repository
                .GetAll()
                .TryFind(c => c.CustomerId == id)
                .Map<Models.Customer, IActionResult>(Ok)
                .Otherwise(NotFound);

        [HttpPost]
        public IActionResult Post([FromBody] Models.Customer customer) =>
            _repository
                .Add(customer)
                .Bind(c => (IActionResult)Created("some link to the new customer", c))
                .Reduce(_ => ApiError.Conflict(), error => error is AlreadyExists<Models.Customer>)
                .Reduce(_ => ApiError.InternalServerError());
    }
}
