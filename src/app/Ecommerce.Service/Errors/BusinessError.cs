﻿namespace Ecommerce.Service.Errors
{    
    public class NotFound<T> : Error
    {
        public string Name { get; }
        public NotFound(string name) => Name = name;
    }

    public class AlreadyExists<T> : Error
    {
        public T Data { get; }
        public AlreadyExists(T data) => Data = data;
    }
}
