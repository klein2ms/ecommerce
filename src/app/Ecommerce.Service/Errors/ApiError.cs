﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Ecommerce.Service.Errors
{
    public static class ApiError
    {
        public static IActionResult Conflict() => new StatusCodeResult(StatusCodes.Status409Conflict);
        public static IActionResult InternalServerError() => new StatusCodeResult(StatusCodes.Status500InternalServerError);
    }
}
