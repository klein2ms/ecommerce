﻿namespace Ecommerce.Service.Errors
{
    public class DatabaseError<T> : Error
    {
        public T Data { get; }
        public string Message { get; }
        public DatabaseError(T data, string message)
        {
            Data = data;
            Message = message;
        }
    }
}
